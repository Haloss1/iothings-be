const express = require("express");
const bodyParser = require("body-parser");
const bcrypt = require("bcrypt");
const cors = require("cors");
const knex = require("knex");
const mqtt = require("mqtt");

const client = mqtt.connect("mqtt://iothings.centralus.cloudapp.azure.com");

client.on("connect", function() {
  client.subscribe("privada01/entrada/pluma", function(err) {
    if (!err) {
      client.publish("privada01/entrada/pluma", "Hello mqtt");
    }
  });
});

const db = knex({
  client: "pg",
  connection: {
    host: "iothings.centralus.cloudapp.azure.com",
    user: "postgres",
    password: "myturbonutterppboye",
    database: "postgres"
  }
});

// db.select("*")
//   .from("users")
//   .then(data => {
//     console.log(data);
//   });

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.get("/", (req, res) => {
  res.json(database.users);
});

app.post("/signin", (req, res) => {
  db.select("email", "hash")
    .where("email", "=", req.body.email)
    .from("login")
    .then(data => {
      const isValid = bcrypt.compareSync(req.body.password, data[0].hash);
      if (isValid) {
        db.select("*")
          .from("users")
          .where("email", "=", req.body.email)
          .then(user => {
            res.json(user[0]);
          })
          .catch(err => res.status(400).json("Unable to get user"));
      } else {
        res.status(400).json("wrong credentials");
      }
    })
    .catch(err => res.status(400).json("wrong credentials"));
});

app.post("/register", (req, res) => {
  const { email, name, password } = req.body;
  const hash = bcrypt.hashSync(password, 10);
  db.transaction(trx => {
    trx
      .insert({
        hash: hash,
        email: email
      })
      .into("login")
      .returning("email")
      .then(loginEmail => {
        return trx("users")
          .returning("*")
          .insert({
            name: name,
            email: loginEmail[0],
            joined: new Date()
          })
          .then(user => {
            res.json(user);
          });
      })
      .then(trx.commit)
      .catch(trx.rollback);
  }).catch(err => res.status(400).json("Unable to register"));
});

app.get("/profile/:id", (req, res) => {
  const { id } = req.params;
  db.select("*")
    .from("users")
    .where({ id })
    .then(user => {
      user.length ? res.json(user[0]) : res.status(400).json("Notfound");
    })
    .catch(err => res.status(400).json("Error getting user"));
});

app.put("/image", (req, res) => {
  const { id } = req.body;
  db("users")
    .where("id", "=", id)
    .increment("entries", 1)
    .returning("entries")
    .then(entries => {
      res.json(entries[0]);
    })
    .catch(err => res.status(400).json("unable to get entries"));
});

app.post("/poweron", (req, res) => {
  client.publish("privada01/entrada/pluma", "1");
  res.status(200).json("success");
});

app.post("/poweroff", (req, res) => {
  client.publish("privada01/entrada/pluma", "0");
  res.status(200).json("success");
});

app.listen(3005, () => {
  console.log("app is running port 3005");
});

/*

/ --> res = this is working
/signin --> POST = success/fail
/register --> POST = user
/profile/:userId --> GET = user
/image --> PUT --> user 

*/
